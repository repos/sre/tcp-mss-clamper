BINARY_NAME = tcp-mss-clamper
GO_GENERATE = go generate
GO_BUILD = go build
GOARCH ?= amd64
GOOS ?= linux
CGO_ENABLED ?= 0
QEMU_GUEST_RAM_SIZE ?= 256

all: build

clean:
	go clean
	rm -f $(BINARY_NAME)

generate:
	$(GO_GENERATE) ./...

build: generate
	CGO_ENABLED=$(CGO_ENABLED) GOARCH=$(GOARCH) GOOS=$(GOOS) $(GO_BUILD) -o $(BINARY_NAME) .

run: dep build
	./$(BINARY_NAME)

dep:
	go mod download

test:
	QEMU_GUEST_RAM_SIZE=$(QEMU_GUEST_RAM_SIZE) go test -v ./...
