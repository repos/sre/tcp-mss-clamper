# TCP MSS Clamper
TCP MSS Clamper is a small eBPF program that performs [TCP MSS Clamping](https://en.wikipedia.org/wiki/MSS_clamping) for a given list of source IPs. It's designed to be used in real servers behind load balancers that forwards traffic to them using IPIP or any other kind of encapsulation where packet size needs to be limited
## Building and testing
### Dependencies
This project needs the following system dependencies:
* clang
* llvm
* libbpf-dev
* golang-1.22
* gcc-multilib
* qemu (used by integration tests)
* bpftool (used by integration tests)

### Build
Building the project should be as easy as running `make` after fetching the dependencies with `make dep`
### Testing
TCP MSS Clamper can be easily tested in the loopback interface with the following commands:
```
sudo tc qdisc add dev lo clsact
sudo ./tcp-mss-clamper -i lo --ipv4-mss 1442 --ipv6-mss 1420 -s 127.0.0.1:1234 -p :8888
```
Spawn nc on lo listening on port 1234, establish a connection while capturing traffic with `tcpdump -vv` and MSS should being clamped. Tcpdump should show the mss being clamped to 1442 for IPv4 traffic like this:
```
tcpdump: listening on lo, link-type EN10MB (Ethernet), snapshot length 262144 bytes
23:46:15.030152 IP (tos 0x0, ttl 64, id 47332, offset 0, flags [DF], proto TCP (6), length 60)
    localhost.46694 > localhost.1234: Flags [S], cksum 0xfe30 (incorrect -> 0x43d9), seq 569337647, win 65495, options [mss 1442,sackOK,TS val 3506499846 ecr 0,nop,wscale 7], length 0
23:46:15.030171 IP (tos 0x0, ttl 64, id 0, offset 0, flags [DF], proto TCP (6), length 60)
    localhost.1234 > localhost.46694: Flags [S.], cksum 0xfe30 (incorrect -> 0xc0f7), seq 2863863329, ack 569337648, win 65483, options [mss 1442,sackOK,TS val 3506499846 ecr 3506499846,nop,wscale 7], length
0
```
#### Integration tests
Integration tests are available as part of the clamper package. `make test` will run the tests as long as `qemu-system-x86_64` is available (`qemu-system-x86` package on debian).

We've detected that some systems using llvm (like Arch) produce big initrd files (~200MB). This could trigger the OOM killer during the unpacking of the initramfs using the default RAM assigned to the QEMU guest, this can be fixed increasing the VM memory size using the env variable `QEMU_GUEST_RAM_SIZE`:
```
make -e QEMU_GUEST_RAM_SIZE=512 test
QEMU_GUEST_RAM_SIZE=512 go test -v ./...
```
## Prometheus endpoint
A prometheus endpoint is provided as long as `-p|--prometheus` flag is used:
```
$ curl 127.0.0.1:8888/metrics -s |grep clamper
# HELP tcp_mss_clamper_mss_cfg Reports configured MSS values per interface and IP version
# TYPE tcp_mss_clamper_mss_cfg gauge
tcp_mss_clamper_mss_cfg{interface="lo",protocol="IPv4"} 1400
tcp_mss_clamper_mss_cfg{interface="lo",protocol="IPv6"} 1400
# HELP tcp_mss_clamper_packets_total Number of packets per interface and state
# TYPE tcp_mss_clamper_packets_total counter
tcp_mss_clamper_packets_total{interface="lo",state="clamped"} 1
tcp_mss_clamper_packets_total{interface="lo",state="failed"} 0
# HELP tcp_mss_clamper_up Reports if the current scrape was successful or not
# TYPE tcp_mss_clamper_up counter
tcp_mss_clamper_up{interface="lo"} 1
```
