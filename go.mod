module gitlab.wikimedia.org/repos/sre/tcp-mss-clamper

go 1.22

toolchain go1.22.1

require (
	github.com/cilium/ebpf v0.11.0
	github.com/florianl/go-tc v0.4.4-0.20240511074908-d584238bf6cb
	github.com/gopacket/gopacket v1.2.0
	github.com/mdlayher/netlink v1.7.2
	github.com/mmatczuk/anyflag v0.0.0-20231026075539-5f42d2f36d96
	github.com/prometheus/client_golang v1.17.0
	github.com/spf13/cobra v1.7.0
	gitlab.wikimedia.org/repos/sre/go-qemutest v0.1.0
	golang.org/x/sys v0.16.0
)

require (
	github.com/beorn7/perks v1.0.1 // indirect
	github.com/cespare/xxhash/v2 v2.2.0 // indirect
	github.com/dustin/go-humanize v1.0.1 // indirect
	github.com/golang/protobuf v1.5.3 // indirect
	github.com/google/go-cmp v0.6.0 // indirect
	github.com/inconshreveable/mousetrap v1.1.0 // indirect
	github.com/josharian/native v1.1.0 // indirect
	github.com/matttproud/golang_protobuf_extensions v1.0.4 // indirect
	github.com/mdlayher/socket v0.5.0 // indirect
	github.com/pierrec/lz4/v4 v4.1.14 // indirect
	github.com/prometheus/client_model v0.4.1-0.20230718164431-9a2bf3000d16 // indirect
	github.com/prometheus/common v0.44.0 // indirect
	github.com/prometheus/procfs v0.11.1 // indirect
	github.com/spf13/pflag v1.0.5 // indirect
	github.com/u-root/u-root v0.12.0 // indirect
	golang.org/x/exp v0.0.0-20231219180239-dc181d75b848 // indirect
	golang.org/x/net v0.19.0 // indirect
	golang.org/x/sync v0.5.0 // indirect
	google.golang.org/protobuf v1.31.0 // indirect
)
