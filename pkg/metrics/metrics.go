package metrics

import (
	"github.com/prometheus/client_golang/prometheus"
	"gitlab.wikimedia.org/repos/sre/tcp-mss-clamper/pkg/clamper"
)

const (
	FAILED_LABEL  = "failed"
	CLAMPED_LABEL = "clamped"
)

var (
	PacketsDesc = prometheus.NewDesc(
		"tcp_mss_clamper_packets_total",
		"Number of packets per interface and state",
		[]string{"interface", "state"}, nil,
	)
	ScraperUpDesc = prometheus.NewDesc(
		"tcp_mss_clamper_up",
		"Reports if the current scrape was successful or not",
		[]string{"interface"}, nil,
	)

	ConfiguredMSSDesc = prometheus.NewDesc(
		"tcp_mss_clamper_mss_cfg",
		"Reports configured MSS values per interface and IP version",
		[]string{"interface", "protocol"}, nil,
	)

	AttachedDesc = prometheus.NewDesc(
		"tcp_mss_clamper_attached",
		"Reports whether it's currently attached to a configured interface",
		[]string{"interface"}, nil,
	)
)

type ClamperMetricCollector struct {
	Programs map[string]*clamper.Clamper
}

func (cmc ClamperMetricCollector) Describe(ch chan<- *prometheus.Desc) {
	prometheus.DescribeByCollect(cmc, ch)
}

func (cmc ClamperMetricCollector) Collect(ch chan<- prometheus.Metric) {
	for ifaceName, program := range cmc.Programs {
		successful := float64(1)
		stats, err := program.GetStats()
		if err != nil {
			successful = float64(0)
		}

		ch <- prometheus.MustNewConstMetric(
			ScraperUpDesc,
			prometheus.CounterValue,
			successful,
			ifaceName)

		if err != nil {
			continue
		}

		ch <- prometheus.MustNewConstMetric(
			PacketsDesc,
			prometheus.CounterValue,
			float64(stats.Failed),
			ifaceName, FAILED_LABEL)

		ch <- prometheus.MustNewConstMetric(
			PacketsDesc,
			prometheus.CounterValue,
			float64(stats.Successful),
			ifaceName, CLAMPED_LABEL)

		ch <- prometheus.MustNewConstMetric(
			ConfiguredMSSDesc,
			prometheus.GaugeValue,
			float64(program.V4MSS),
			ifaceName, "IPv4")

		ch <- prometheus.MustNewConstMetric(
			ConfiguredMSSDesc,
			prometheus.GaugeValue,
			float64(program.V6MSS),
			ifaceName, "IPv6")

		attached := float64(1)
		if !program.Attached {
			attached = float64(0)
		}

		ch <- prometheus.MustNewConstMetric(
			AttachedDesc,
			prometheus.GaugeValue,
			attached,
			ifaceName)
	}
}
