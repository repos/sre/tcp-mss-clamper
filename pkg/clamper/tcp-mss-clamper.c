//go:build ignore
/*
 * SPDX-License-Identifier: GPL-2.0
 * Copyright 2023-present Valentin Gutierrez
 * Copyright 2023-present Wikimedia Foundation, Inc.
*/
#include <linux/bpf.h>
#include <linux/if_ether.h>
#include <linux/in.h>
#include <linux/ip.h>
#include <linux/ipv6.h>
#include <linux/tcp.h>
#include <linux/pkt_cls.h>
#include <bpf/bpf_helpers.h>
#include <bpf/bpf_endian.h>

#define MAX_ALLOWED_SOURCES 8
#define STATS_SIZE 1
#define STATS_INDEX 0
#define CONFIG_SIZE 1
#define CONFIG_INDEX 0
#define NO_FLAGS 0
#define MAX_TCP_OPTIONS 10

#define TCP4_CSUM_OFF (ETH_HLEN + sizeof(struct iphdr) + offsetof(struct tcphdr, check))
#define TCP6_CSUM_OFF (ETH_HLEN + sizeof(struct ipv6hdr) + offsetof(struct tcphdr, check))

struct config {
    __be16 v4_mss;
    __be16 v6_mss;
};

struct {
    __uint(type, BPF_MAP_TYPE_PERCPU_ARRAY);
    __type(key, __u32);
    __type(value, struct config);
    __uint(max_entries, CONFIG_SIZE);
    __uint(map_flags, NO_FLAGS);
} config_map SEC(".maps");

struct stats {
    __u64 failed;
    __u64 successful;
};

struct {
    __uint(type, BPF_MAP_TYPE_PERCPU_ARRAY);
    __type(key, __u32);
    __type(value, struct stats);
    __uint(max_entries, STATS_SIZE);
    __uint(map_flags, NO_FLAGS);
} stats_map SEC(".maps");

struct v4_source {
    __be32 address;
    __be16 port;
};

// v4 map
struct {
    __uint(type, BPF_MAP_TYPE_PERCPU_ARRAY);
    __type(key, __u32);
    __type(value, struct v4_source);
    __uint(max_entries, MAX_ALLOWED_SOURCES);
    __uint(map_flags, 0);
} v4_sources_map SEC(".maps");

struct v6_source {
    struct in6_addr address;
    __be16 port;
};

// v6 map
struct {
    __uint(type, BPF_MAP_TYPE_PERCPU_ARRAY);
    __type(key, __u32);
    __type(value, struct v6_source);
    __uint(max_entries, MAX_ALLOWED_SOURCES);
} v6_sources_map SEC(".maps");


struct callback_ctx {
    union {
        __be32 srcip;
        struct in6_addr *srcipv6;
    };
    __be16 port;
    __u8 found;
};

// callbacks for bpf_for_each_map_elem()
static long check_source_ip(void *map, const __u32 *key, struct v4_source *val,
        struct callback_ctx *data) {
    if (val->address == data->srcip && val->port == data->port) {
        data->found = 1;
        return 1;
    }
    return 0;
}
static long check_source_ip6(void *map, const __u32 *key, struct v6_source *val,
        struct callback_ctx *data) {
      if (val->port == data->port && ((val->address.s6_addr32[0] ^ data->srcipv6->s6_addr32[0]) |
        (val->address.s6_addr32[1] ^ data->srcipv6->s6_addr32[1]) |
        (val->address.s6_addr32[2] ^ data->srcipv6->s6_addr32[2]) |
        (val->address.s6_addr32[3] ^ data->srcipv6->s6_addr32[3])) == 0) {
            data->found = 1;
            return 1;
    }
    return 0;
}

// Remove in favor of bpf_for_each_map_elem as soon as we only need to support >=bookworm (6.1 kernels)
static long loop_map_v4(void *map, long (*callback_fn)(void*, void*, void *, void *), void *callback_ctx, __u64 flags) {
    __u32 key = 0;
    __u32 i = 0;
    struct v4_source* source = NULL;
    for (i = 0; i < MAX_ALLOWED_SOURCES; i++) {
        key = i;
        source = bpf_map_lookup_elem(map, &key);
        if (!source) {
            continue;
        }
        __u64 ret = callback_fn(&v4_sources_map, &key, source, callback_ctx);
        if (ret == 1) {
            return i;
        }
    }
    return i;
}

static long loop_map_v6(void *map, long (*callback_fn)(void*, void*, void *, void *), void *callback_ctx, __u64 flags) {
    __u32 key = 0;
    __u32 i = 0;
    struct v6_source* source = NULL;
    for (i = 0; i < MAX_ALLOWED_SOURCES; i++) {
        key = i;
        source = bpf_map_lookup_elem(map, &key);
        if (!source) {
            continue;
        }
        __u64 ret = callback_fn(&v4_sources_map, &key, source, callback_ctx);
        if (ret == 1) {
            return i;
        }
    }
    return i;
}

// adapted code from https://github.com/xdp-project/bpf-examples/blob/63cd4007b16b49b44241f17a38f26aea926f8afc/pping/pping_kern.c#L424
static int find_mss(struct tcphdr *tcph, void *data_end, void **msspos) {
    int len = tcph->doff << 2;
    void *opt_end = (void *)tcph + len;
    __u8 *pos = (__u8 *)(tcph + 1); //Current pos in TCP options
    __u8 i, opt;
    volatile __u8
        opt_size; // Seems to ensure it's always read of from stack as u8

    if ((void *)tcph + 1 > data_end || len <= sizeof(struct tcphdr))
        return -1;
#pragma unroll //temporary solution until we can identify why the non-unrolled loop gets stuck in an infinite loop
    for (i = 0; i < MAX_TCP_OPTIONS; i++) {
        if ((void *)pos + 1 > opt_end || (void *)pos + 1 > data_end)
            return -1;

        opt = *pos;
        if (opt == 0) // Reached end of TCP options
            return -1;

        if (opt == 1) { // TCP NOP option - advance one byte
            pos++;
            continue;
        }

        // Option > 1, should have option size
        if ((void *)pos + 2 > opt_end || (void *)pos + 2 > data_end)
            return -1;
        opt_size = *(pos + 1);
        if (opt_size < 2) // Stop parsing options if opt_size has an invalid value
            return -1;

        // Option-kind is TCP MSS
        if (opt == 2 && opt_size == 4) {
            if ((void *)pos + 4 > opt_end || (void *)pos + 4 > data_end)
                return -1;
            *msspos = pos+2;
            return 0;
        }

        // Some other TCP option - advance option-length bytes
        pos += opt_size;
    }
    return -1;
}


SEC("tc")
int tcp_mss_clamper(struct __sk_buff *skb) {
    void *data = (void *)(long)skb->data;
    void *data_end = (void *)(long)skb->data_end;
    struct ethhdr *eth = data;
    struct callback_ctx ctx;
    struct config *config;
    struct stats *stats;
    __u32 config_key = CONFIG_INDEX;
    __u32 stats_key = STATS_INDEX;

    config = bpf_map_lookup_elem(&config_map, &config_key);
    if (!config) {
        return TC_ACT_UNSPEC;
    }

    stats = bpf_map_lookup_elem(&stats_map, &stats_key);
    if (!stats) {
        return TC_ACT_UNSPEC;
    }

    __u8 vip_found = 0;
    __u16 h_proto = 0;

    ctx.found = 0;


    if (data + sizeof(*eth) > data_end)
       return TC_ACT_UNSPEC;

    h_proto = bpf_ntohs(eth->h_proto);

    if (h_proto == ETH_P_IP) { // IPv4
        if (data + sizeof(*eth) + sizeof(struct iphdr) > data_end) {
            return TC_ACT_UNSPEC;
        }
        struct iphdr *iph = data + sizeof(*eth);

        if ((void*)(iph + 1) > data_end || iph->protocol != IPPROTO_TCP) {
            return TC_ACT_UNSPEC;
        }
        struct tcphdr *tcph = (struct tcphdr *)(iph +1);
        if ((void *)(tcph + 1) > data_end) {
            return TC_ACT_UNSPEC;
        }

        if (tcph->syn) { // this looks almost like a copy&paste of the IPv6 block ahead
                         // but the verifier isn't happy with my attempts of reusing the code
            void *msspos = NULL;
            find_mss(tcph, data_end, &msspos);
            ctx.srcip = iph->saddr;
            ctx.port = tcph->source;
//          bpf_for_each_map_elem(&v4_sources_map, check_source_ip, &ctx, 0);
            loop_map_v4(&v4_sources_map, check_source_ip, &ctx, 0);
            vip_found = ctx.found;
            if (vip_found && msspos) {
                __be16 new_mss = config->v4_mss;
                __be16 current_mss = *(__be16*)msspos;
                if (new_mss < current_mss) {
                    bpf_skb_store_bytes(skb, msspos - data, &new_mss, sizeof(new_mss), BPF_F_RECOMPUTE_CSUM);
                    stats->successful++;
                }
            } else if(vip_found && !msspos) {
                // this shouldn't happen as the linux kernel doesn't provide a way of
                // disabling MSS injection
                stats->failed++;
            }
        }
    } else if (h_proto == ETH_P_IPV6) { // IPv6
        if (data + sizeof(*eth) + sizeof(struct ipv6hdr) > data_end) {
            return TC_ACT_UNSPEC;
        }
        struct ipv6hdr *ip6h = data + sizeof(*eth);

        if ((void*)(ip6h + 1) > data_end || ip6h->nexthdr != IPPROTO_TCP) {
            return TC_ACT_UNSPEC;
        }
        struct tcphdr *tcph = (struct tcphdr *)(ip6h +1);
        if ((void *)(tcph + 1) > data_end) {
            return TC_ACT_UNSPEC;
        }

        if (tcph->syn) {
            void *msspos = NULL;
            find_mss(tcph, data_end, &msspos);
            ctx.srcipv6 = &ip6h->saddr;
            ctx.port = tcph->source;
            // bpf_for_each_map_elem(&v6_sources_map, check_source_ip6, &ctx, 0);
            loop_map_v6(&v6_sources_map, check_source_ip6, &ctx, 0);
            vip_found = ctx.found;
            if (vip_found && msspos) {
                __be16 new_mss = config->v6_mss;
                __be16 current_mss = *(__be16*)msspos;
                if (new_mss < current_mss) {
                    bpf_skb_store_bytes(skb, msspos - data, &new_mss, sizeof(new_mss), BPF_F_RECOMPUTE_CSUM);
                    stats->successful++;
                }
            } else if(vip_found && !msspos) {
                // this shouldn't happen as the linux kernel doesn't provide a way of
                // disabling MSS injection
                stats->failed++;
            }
        }
    } else {
        return TC_ACT_UNSPEC;
    }

    return TC_ACT_UNSPEC;
}

char _license[] SEC("license") = "GPL";
