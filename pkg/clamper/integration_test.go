//go:build linux
// +build linux

/*
Copyright 2023 Wikimedia Foundation Inc.
Copyright 2018 Google Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

	http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package clamper

import (
	"context"
	"encoding/binary"
	"log"
	"net"
	"net/netip"
	"os"
	"os/exec"
	"path/filepath"
	"testing"
	"time"

	"golang.org/x/sys/unix"

	"github.com/florianl/go-tc"
	"github.com/florianl/go-tc/core"
	"github.com/gopacket/gopacket"
	"github.com/gopacket/gopacket/layers"
	"github.com/gopacket/gopacket/pcap"

	"gitlab.wikimedia.org/repos/sre/go-qemutest/pkg/qemutest"

	"gitlab.wikimedia.org/repos/sre/tcp-mss-clamper/pkg/testsutils"
)

const (
	NETWORK_INTERFACE = "lo"
	SOURCE_IPV4       = "127.0.0.1"
	SOURCE_IPV6       = "[::1]"
	SOURCE_PORT       = "12345"
	MSS               = 1400
)

// binaries that will be shipped on initrd
var initrdBinaries = []string{"/usr/sbin/bpftool"}

type QemuTest struct{}

func TestMain(m *testing.M) {
	if qemutest.InQemu() {
		// pinning requires /sys/fs/bpf
		if err := unix.Mount("bpf", "/sys/fs/bpf", "bpf", 0, ""); err != nil {
			log.Fatalf("failed to mount /sys/fs/bpf: %v", err)
		}
	}
	os.Exit(qemutest.QemuTestMain(m))
}

func TestQemu(t *testing.T) {
	if qemutest.InQemu() {
		qemutest.RunQemuTests(t, QemuTest{})
		return
	}
	tdPath, err := testsutils.GetTestdata()
	if err != nil {
		t.Fatalf("unable to find Testdata path: %v", err)
	}

	testCases := []qemutest.QemuTestCase{
		{
			KernelImage: filepath.Join(tdPath, "bzImage.6.1"),
			TestId:      "6.1.69-bookworm",
			Verbose:     true,
		},
		{
			KernelImage: filepath.Join(tdPath, "bzImage.5.10"),
			TestId:      "5.10.197-bullseye",
			Verbose:     true,
		},
	}

	qemutest.RunQemu(t, testCases, initrdBinaries)
}

// Simple TCP listener that stops listening after one successful connection
func tcpListener(address, port string) {
	l, err := net.Listen("tcp", address+":"+port)
	if err != nil {
		log.Fatalf("Unable to spawn a TCP listener on %s:%s: %v",
			address, port, err)
	}
	defer l.Close()
	conn, err := l.Accept()
	if err != nil {
		log.Fatalf("Unable to accept TCP connection: %v", err)
	}
	conn.Close() // we are only interested in performing a single TCP handshake
}

func tcpClient(address, port string) error {
	const max_attempts = 16
	var d net.Dialer
	var err error
	ctx, cancel := context.WithTimeout(context.Background(), time.Second)
	defer cancel()

	for i := 0; i < max_attempts; i++ {
		conn, err := d.DialContext(ctx, "tcp", address+":"+port)
		if err != nil {
			time.Sleep(time.Duration(i) * time.Millisecond)
			continue
		}
		conn.Close()
		return nil
	}
	return err
}

// capture the SYN/ACK response and report the MSS to the specified channel
func getMss(port string, c chan uint16, ready chan bool) {
	defer close(c)
	defer close(ready)
	handle, err := pcap.OpenLive(NETWORK_INTERFACE, 262144, true, 1*time.Second)
	if err != nil {
		panic(err)
	}
	defer handle.Close()

	if err := handle.SetBPFFilter("port " + port); err != nil {
		panic(err)
	}

	ctx, cancel := context.WithTimeout(context.Background(), 2*time.Second)
	defer cancel()
	packets := gopacket.NewPacketSource(
		handle, handle.LinkType()).PacketsCtx(ctx)
	ready <- true
	for pkt := range packets {
		tcpLayer := pkt.Layer(layers.LayerTypeTCP)
		if tcpLayer == nil {
			continue
		}
		tcp := tcpLayer.(*layers.TCP)
		if tcp.SYN && tcp.ACK {
			for _, option := range tcp.Options {
				if option.OptionType == layers.TCPOptionKindMSS {
					c <- binary.BigEndian.Uint16(option.OptionData)
					return
				}
			}
		}
	}
}

func isEbpfLoaded(t *testing.T, expect bool) bool {
	programFound := false
	pollingTime := 10 * time.Millisecond
	for i := time.Duration(0); i < time.Second; i += pollingTime {
		cmd := exec.Command("/usr/sbin/bpftool", "prog", "list", "name", "tcp_mss_clamper")
		_, err := cmd.CombinedOutput()
		if err == nil {
			programFound = true
		}
		if programFound && expect {
			return true
		}
		if !programFound && !expect {
			return false
		}
		time.Sleep(pollingTime)
	}
	if programFound {
		return true
	}
	if !programFound {
		t.Error("eBPF program not loaded after 1 second")
	}
	return false
}

func (QemuTest) TestLoadingProgram(t *testing.T) {
	// Before being able to use the eBPF program clsact qdisc
	// needs to be attached to the network interface
	iface, err := net.InterfaceByName(NETWORK_INTERFACE)
	if err != nil {
		t.Errorf("unable to find interface %s: %v", NETWORK_INTERFACE, err)
	}
	tcnl, err := tc.Open(&tc.Config{})
	if err != nil {
		t.Fatalf("unable to open rtnetlink socket: %v", err)
	}
	qdisc := tc.Object{
		Msg: tc.Msg{
			Family:  unix.AF_UNSPEC,
			Ifindex: uint32(iface.Index),
			Handle:  core.BuildHandle(tc.HandleRoot, 0x0000),
			Parent:  tc.HandleIngress,
			Info:    0,
		},
		Attribute: tc.Attribute{
			Kind: "clsact",
		},
	}
	if err := tcnl.Qdisc().Add(&qdisc); err != nil {
		t.Errorf("unable to attach qdisc: %v\n", err)
	}
	defer tcnl.Qdisc().Delete(&qdisc)

	t.Log("Loading eBPF program")
	cl := NewClamper(MSS, MSS, iface)
	if err := cl.Init(); err != nil {
		t.Fatalf("Unable to load eBPF program: %v", err)
	}
	for _, source := range []string{SOURCE_IPV4, SOURCE_IPV6} {
		source := netip.MustParseAddrPort(source + ":" + SOURCE_PORT)
		cl.AddSource(source)
	}

	// Check externally using bpftool
	if !isEbpfLoaded(t, true) {
		t.Error("eBPF program should be loaded")
	}

	for _, source := range []string{SOURCE_IPV4, SOURCE_IPV6} {
		c := make(chan uint16)
		readyChan := make(chan bool)
		go getMss(SOURCE_PORT, c, readyChan)
		<-readyChan // wait till getMss() is ready to capture packets
		go tcpListener(source, SOURCE_PORT)
		if err := tcpClient(source, SOURCE_PORT); err != nil {
			t.Errorf("unable to establish a tcp connection against the test service %s:%s: %v", source, SOURCE_PORT, err)
		}
		if mss := <-c; mss != MSS {
			t.Errorf("unexpected MSS %v for %s:%s", mss, source, SOURCE_PORT)
		}
	}
	cl.Close()
	if isEbpfLoaded(t, false) {
		t.Error("eBPF shouldn't be loaded after Close()")
	}
}

func (QemuTest) TestPin(t *testing.T) {
	iface, err := net.InterfaceByName(NETWORK_INTERFACE)
	if err != nil {
		t.Errorf("unable to find interface %s: %v", NETWORK_INTERFACE, err)
	}
	tcnl, err := tc.Open(&tc.Config{})
	if err != nil {
		t.Fatalf("unable to open rtnetlink socket: %v", err)
	}
	qdisc := tc.Object{
		Msg: tc.Msg{
			Family:  unix.AF_UNSPEC,
			Ifindex: uint32(iface.Index),
			Handle:  core.BuildHandle(tc.HandleRoot, 0x0000),
			Parent:  tc.HandleIngress,
			Info:    0,
		},
		Attribute: tc.Attribute{
			Kind: "clsact",
		},
	}
	if err := tcnl.Qdisc().Add(&qdisc); err != nil {
		t.Errorf("unable to attach qdisc: %v\n", err)
	}
	defer tcnl.Qdisc().Delete(&qdisc)

	t.Log("Loading eBPF program")
	cl := NewClamper(MSS, MSS, iface)
	if err := cl.Init(); err != nil {
		t.Fatalf("Unable to load eBPF program: %v", err)
	}
	for _, source := range []string{SOURCE_IPV4, SOURCE_IPV6} {
		source := netip.MustParseAddrPort(source + ":" + SOURCE_PORT)
		cl.AddSource(source)
	}

	// Pin the program
	if err := cl.Pin(); err != nil {
		t.Errorf("unable to pin: %v", err)
	}
	cl.Close()

	if !isEbpfLoaded(t, true) {
		t.Error("eBPF program isn't loaded after Pin() + Close()")
	}

	for _, source := range []string{SOURCE_IPV4, SOURCE_IPV6} {
		c := make(chan uint16)
		readyChan := make(chan bool)
		go getMss(SOURCE_PORT, c, readyChan)
		<-readyChan // wait till getMss() is ready to capture packets
		go tcpListener(source, SOURCE_PORT)
		if err := tcpClient(source, SOURCE_PORT); err != nil {
			t.Errorf("unable to establish a tcp connection against the test service %s:%s: %v", source, SOURCE_PORT, err)
		}
		if mss := <-c; mss != MSS {
			t.Errorf("unexpected MSS %v for %s:%s", mss, source, SOURCE_PORT)
		}
	}

	// Test MSS reconfiguration
	cl = NewClamper(MSS-1, MSS-1, iface)
	if err := cl.Init(); err != nil {
		t.Fatalf("unable to init clamper instance: %v", err)
		return
	}
	for _, source := range []string{SOURCE_IPV4, SOURCE_IPV6} {
		c := make(chan uint16)
		readyChan := make(chan bool)
		go getMss(SOURCE_PORT, c, readyChan)
		<-readyChan // wait till getMss() is ready to capture packets
		go tcpListener(source, SOURCE_PORT)
		if err := tcpClient(source, SOURCE_PORT); err != nil {
			t.Errorf("unable to establish a tcp connection against the test service %s:%s: %v", source, SOURCE_PORT, err)
		}
		if mss := <-c; mss != MSS-1 {
			t.Errorf("unexpected MSS %v for %s:%s", mss, source, SOURCE_PORT)
		}
	}
	cl.Unpin()
	cl.Close()

	if isEbpfLoaded(t, false) {
		t.Error("eBPF program is still lodade after Unpin() + Close()")
	}
}
