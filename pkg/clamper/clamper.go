package clamper

import (
	"encoding/binary"
	"errors"
	"fmt"
	"log"
	"net"
	"net/netip"
	"os"
	"path/filepath"

	"github.com/cilium/ebpf"
	"github.com/florianl/go-tc"
	"github.com/florianl/go-tc/core"
	"github.com/mdlayher/netlink"
	"gitlab.wikimedia.org/repos/sre/tcp-mss-clamper/pkg/cpu"
	"golang.org/x/sys/unix"
)

//go:generate go run github.com/cilium/ebpf/cmd/bpf2go -cc clang -cflags -O2 clamper tcp-mss-clamper.c

const (
	configIndex = uint32(0)
	statsIndex  = uint32(0)
	bpfName     = "tcp-mss-clamper"
	basePinPath = "/sys/fs/bpf"
)

var (
	ErrorBPFNotLoaded         = errors.New("BPF program not loaded")
	ErrorOptimizerNotAttached = errors.New("Not attached to any interface")
	ErrorMaxEntriesReached    = errors.New("VIP array is already full")
)

type Clamper struct {
	objs         clamperObjects
	iface        *net.Interface
	loaded       bool
	Attached     bool
	pinned       bool
	programFD    int
	tcnl         *tc.Tc
	v4Index      uint32
	v6Index      uint32
	v4MaxEntries uint32
	v6MaxEntries uint32
	V4MSS        uint16
	V6MSS        uint16
	cpus         int
}

func NewClamper(v4MSS, v6MSS uint16, iface *net.Interface) *Clamper {
	cpus, err := cpu.PossibleCPUs()
	if err != nil {
		log.Fatalf("Unable to fetch the number of CPUs: %v", err)
	}

	return &Clamper{
		objs:     clamperObjects{},
		loaded:   false,
		Attached: false,
		pinned:   false,
		cpus:     cpus,
		V4MSS:    v4MSS,
		V6MSS:    v6MSS,
		iface:    iface,
	}
}

func (cl *Clamper) load() error {
	programPinPath := cl.programPinPath()
	if _, err := os.Stat(programPinPath); err != nil {
		// clean slate
		spec, err := loadClamper()
		if err != nil {
			return err
		}

		return spec.LoadAndAssign(&cl.objs, nil)
	}

	var err error
	cl.objs.TcpMssClamper, err = ebpf.LoadPinnedProgram(programPinPath, nil)
	if err != nil {
		log.Printf("unable to load pinned program: %s", err)
		return err
	}
	maps := []struct {
		name    string
		ebpfMap **ebpf.Map
	}{
		{fmt.Sprintf("config_map-%s", cl.iface.Name), &cl.objs.ConfigMap},
		{fmt.Sprintf("stats_map-%s", cl.iface.Name), &cl.objs.StatsMap},
		{fmt.Sprintf("v4_sources_map-%s", cl.iface.Name), &cl.objs.V4SourcesMap},
		{fmt.Sprintf("v6_sources_map-%s", cl.iface.Name), &cl.objs.V6SourcesMap},
	}

	for _, m := range maps {
		path := filepath.Join(basePinPath, m.name)
		if os.Stat(path); err != nil {
			return err
		}

		var err error
		*m.ebpfMap, err = ebpf.LoadPinnedMap(path, &ebpf.LoadPinOptions{})
		if err != nil {
			return err
		}
	}

	cl.pinned = true
	return nil
}

func (cl *Clamper) Init() error {
	// load eBPF program
	if err := cl.load(); err != nil {
		log.Printf("Unable to load eBPF program: %v: %v\n", cl.objs, err)
		return err
	}
	cl.programFD = cl.objs.TcpMssClamper.FD()
	log.Printf("eBPF program supports up to %v sources", cl.objs.V4SourcesMap.MaxEntries())
	// Fetch BPF Maps capacities
	cl.v4MaxEntries = cl.objs.V4SourcesMap.MaxEntries()
	cl.v6MaxEntries = cl.objs.V6SourcesMap.MaxEntries()
	cl.loaded = true

	if cl.pinned {
		var err error
		cl.Attached, err = cl.isAttached()
		if err != nil {
			return err
		}
		v4MSS, v6MSS, err := cl.GetMSS()
		if err != nil {
			return err
		}

		reconfigure := false
		if v4MSS != cl.V4MSS {
			log.Printf("Detected new IPv4 MSS value, reconfiguring eBPF program. Old value: %d, new value: %d", v4MSS, cl.V4MSS)
			reconfigure = true
		}
		if v6MSS != cl.V6MSS {
			log.Printf("Detected new IPv6 MSS value, reconfiguring eBPF program. Old value: %d, new value: %d", v6MSS, cl.V6MSS)
			reconfigure = true
		}
		if reconfigure {
			cl.SetMSS(cl.V4MSS, cl.V6MSS)
		}
		cl.restoreIndexes()
	} else {
		cl.SetMSS(cl.V4MSS, cl.V6MSS)
		cl.AttachToInterface()
	}

	return nil
}

// Attach eBPF program to a TC egress clsact qdisc
func (cl *Clamper) AttachToInterface() error {
	if !cl.loaded {
		return ErrorBPFNotLoaded
	}

	if cl.Attached {
		return fmt.Errorf("already attached to %v", cl.iface.Name)
	}

	if !isClsactPresent(*cl.iface) {
		return fmt.Errorf("clsact qdisc not found on %v", cl.iface.Name)
	}

	var err error
	cl.tcnl, err = tc.Open(&tc.Config{})
	if err != nil {
		log.Printf("could not open rtnetlink socket: %v\n", err)
		return err
	}

	fd := uint32(cl.programFD)
	flags := uint32(0x1)
	name := bpfName

	filter := tc.Object{
		tc.Msg{
			Family:  unix.AF_UNSPEC,
			Ifindex: uint32(cl.iface.Index),
			Handle:  0,
			Parent:  core.BuildHandle(tc.HandleRoot, tc.HandleMinEgress),
			Info:    0x300,
		},
		tc.Attribute{
			Kind: "bpf",
			BPF: &tc.Bpf{
				FD:    &fd,
				Flags: &flags,
				Name:  &name,
			},
		},
	}

	log.Printf("Attaching TCP MSS clamper on %v", cl.iface.Name)
	if err := cl.tcnl.Filter().Add(&filter); err != nil {
		log.Printf("could not attach filter for eBPF program: %v\n", err)
		cl.tcnl.Close()
		return err
	}

	cl.Attached = true
	return nil
}

func (cl *Clamper) Detach() error {
	if !cl.Attached {
		return ErrorOptimizerNotAttached
	}

	msg := tc.Msg{
		Family:  unix.AF_UNSPEC,
		Ifindex: uint32(cl.iface.Index),
		Handle:  0,
		Parent:  core.BuildHandle(tc.HandleRoot, tc.HandleMinEgress),
	}

	filters, err := cl.tcnl.Filter().Get(&msg)
	if err != nil {
		log.Printf("Unable to fetch filters: %v\n", err)
		return err
	}

	removed := false
	for _, filter := range filters {
		if filter.Attribute.Kind == "bpf" && filter.Attribute.BPF != nil {
			if *filter.Attribute.BPF.Name == bpfName {
				log.Printf("Detaching TCP MSS clamper from %v", cl.iface.Name)
				if err := cl.tcnl.Filter().Delete(&filter); err != nil {
					return err
				}
				removed = true
			}
		}
	}
	if !removed {
		return fmt.Errorf("Unable to find TC BPF filter %v on interface %v\n", bpfName, cl.iface.Name)
	}

	cl.iface = nil
	cl.Attached = false
	return nil
}

// Add a source (IP:port) to be clamped
func (cl *Clamper) AddSource(addrPort netip.AddrPort) error {
	if !cl.loaded {
		return ErrorBPFNotLoaded
	}

	sourceAddr := addrPort.Addr()
	sourcePort := uint16LEToBE(addrPort.Port())
	v4 := sourceAddr.Is4()
	index := uint32(0)

	if v4 {
		if cl.v4Index >= cl.v4MaxEntries {
			return ErrorMaxEntriesReached
		}
		index = cl.v4Index
	} else {
		if cl.v6Index >= cl.v6MaxEntries {
			return ErrorMaxEntriesReached
		}
		index = cl.v6Index
	}
	sourceBytes := sourceAddr.AsSlice()

	if v4 {
		// we need to provide a slice with the address per CPU
		value := make([]clamperV4Source, cl.cpus)
		for i := range value {
			// use binary.NativeEndian when golang 1.21 is available
			value[i].Address = binary.LittleEndian.Uint32(sourceBytes)
			value[i].Port = sourcePort
		}
		if err := cl.objs.V4SourcesMap.Put(index, value); err != nil {
			log.Printf("Unable to update v4 sources map: %v\n", err)
			return err
		}
		cl.v4Index += 1
	} else {
		// we need to provide a slice with the address per CPU
		value := make([]clamperV6Source, cl.cpus)
		for i := range value {
			copy(value[i].Address.In6U.U6Addr8[:], sourceBytes)
			value[i].Port = sourcePort
		}
		if err := cl.objs.V6SourcesMap.Put(index, value); err != nil {
			log.Printf("Unable to update v6 sources map: %v\n", err)
			return err
		}
		cl.v6Index += 1
	}

	return nil
}

// Restore v4Index and v6Index values
func (cl *Clamper) restoreIndexes() error {
	if !cl.loaded {
		return ErrorBPFNotLoaded
	}
	v4Source := make([]clamperV4Source, cl.cpus)
	for i := uint32(0); i < cl.v4MaxEntries; i++ {
		err := cl.objs.V4SourcesMap.Lookup(i, &v4Source)
		if err != nil {
			return err
		}
		if v4Source[0].Address == 0 && v4Source[0].Port == 0 {
			cl.v4Index = i
			break
		}
	}
	v6Source := make([]clamperV6Source, cl.cpus)
	for i := uint32(0); i < cl.v6MaxEntries; i++ {
		err := cl.objs.V6SourcesMap.Lookup(i, &v6Source)
		if err != nil {
			return err
		}
		used := false
		for _, octet := range v6Source[0].Address.In6U.U6Addr8 {
			if octet != 0 {
				used = true
				break
			}
		}
		if !used && v6Source[0].Port == 0 {
			cl.v6Index = i
			break
		}

	}
	return nil
}

// fetch MSS values from eBPF config map
func (cl *Clamper) GetMSS() (v4MSS uint16, v6MSS uint16, err error) {
	if !cl.loaded {
		return 0, 0, ErrorBPFNotLoaded
	}

	value := make([]clamperConfig, cl.cpus)

	if err := cl.objs.ConfigMap.Lookup(configIndex, &value); err != nil {
		log.Printf("unable to fetch config map: %v\n", err)
		return 0, 0, err
	}

	return uint16BEtoLE(value[0].V4Mss), uint16BEtoLE(value[0].V6Mss), nil
}

// Set MSS
func (cl *Clamper) SetMSS(v4MSS, v6MSS uint16) error {
	if !cl.loaded {
		return ErrorBPFNotLoaded
	}

	v4MSSBe := uint16LEToBE(v4MSS)
	v6MSSBe := uint16LEToBE(v6MSS)

	// we need to provide a slice with the config values per CPU
	value := make([]clamperConfig, cl.cpus)
	for i := range value {
		value[i] = clamperConfig{
			V4Mss: v4MSSBe,
			V6Mss: v6MSSBe,
		}
	}

	if err := cl.objs.ConfigMap.Put(configIndex, value); err != nil {
		log.Printf("unable to update config map: %v\n", err)
		return err
	}

	cl.V4MSS = v4MSS
	cl.V6MSS = v6MSS

	return nil
}

// Fetch eBPF program stats
func (cl *Clamper) GetStats() (clamperStats, error) {
	var stats clamperStats
	if !cl.loaded {
		return stats, ErrorBPFNotLoaded
	}

	var value []clamperStats

	if err := cl.objs.StatsMap.Lookup(statsIndex, &value); err != nil {
		return stats, err
	}

	// StatsMap is a per cpu array
	for _, cpuStats := range value {
		stats.Failed += cpuStats.Failed
		stats.Successful += cpuStats.Successful
	}

	return stats, nil
}

func (cl *Clamper) Close() {
	if cl.Attached {
		if !cl.objs.TcpMssClamper.IsPinned() {
			cl.Detach()
		}
		cl.tcnl.Close()
	}

	if cl.loaded {
		cl.objs.Close()
	}
}

func (cl *Clamper) programPinPath() string {
	return filepath.Join(basePinPath, fmt.Sprintf("tcp-mss-clamper-%s", cl.iface.Name))
}

// Pin the required eBPF maps and programs to be able to restart the control
// plane without stopping MSS clampling
func (cl *Clamper) Pin() error {
	if !cl.loaded {
		return ErrorBPFNotLoaded
	}
	programPinPath := cl.programPinPath()
	if err := cl.objs.TcpMssClamper.Pin(programPinPath); err != nil {
		return err
	}

	maps := []struct {
		name    string
		ebpfMap *ebpf.Map
	}{
		{fmt.Sprintf("config_map-%s", cl.iface.Name), cl.objs.ConfigMap},
		{fmt.Sprintf("stats_map-%s", cl.iface.Name), cl.objs.StatsMap},
		{fmt.Sprintf("v4_sources_map-%s", cl.iface.Name), cl.objs.V4SourcesMap},
		{fmt.Sprintf("v6_sources_map-%s", cl.iface.Name), cl.objs.V6SourcesMap},
	}

	for _, m := range maps {
		path := filepath.Join(basePinPath, m.name)
		if err := m.ebpfMap.Pin(path); err != nil {
			return err
		}
	}

	return nil
}

// Unpin eBPF maps and programs to be able to perform a complete shutdown
func (cl *Clamper) Unpin() error {
	if err := cl.objs.TcpMssClamper.Unpin(); err != nil {
		return err
	}
	maps := []*ebpf.Map{
		cl.objs.ConfigMap,
		cl.objs.StatsMap,
		cl.objs.V4SourcesMap,
		cl.objs.V6SourcesMap,
	}
	for _, m := range maps {
		if err := m.Unpin(); err != nil {
			return err
		}
	}
	return nil
}

// Check if eBPF program is attached to the configured egress interface
func (cl *Clamper) isAttached() (bool, error) {
	var err error
	cl.tcnl, err = tc.Open(&tc.Config{})
	if err != nil {
		log.Printf("Could not open rtlnetlink socket: %v\n", err)
		return false, err
	}

	msg := tc.Msg{
		Family:  unix.AF_UNSPEC,
		Ifindex: uint32(cl.iface.Index),
		Handle:  0,
		Parent:  core.BuildHandle(tc.HandleRoot, tc.HandleMinEgress),
		// Info isn't set here because the value used on AttachToInterface isn't preserved
	}

	filters, err := cl.tcnl.Filter().Get(&msg)
	if err != nil {
		log.Printf("Unable to fetch filters: %v\n", err)
		return false, err
	}

	for _, filter := range filters {
		if filter.Attribute.Kind == "bpf" && filter.Attribute.BPF != nil {
			if *filter.Attribute.BPF.Name == bpfName {
				return true, nil
			}
		}
	}
	return false, nil
}

// Check if clsact (https://lwn.net/Articles/671458/) is present
// on a specific interface
func isClsactPresent(netInterface net.Interface) bool {
	// open a rtnetlink socket
	rtnl, err := tc.Open(&tc.Config{})
	if err != nil {
		log.Printf("could not open rtnetlink socket: %v\n", err)
		return false
	}
	defer func() {
		if err := rtnl.Close(); err != nil {
			log.Printf("could not close rtnetlink socket: %v\n", err)
		}
	}()

	// For enhanced error messages from the kernel, it is recommended to set
	// option `NETLINK_EXT_ACK`, which is supported since 4.12 kernel.
	//
	// If not supported, `unix.ENOPROTOOPT` is returned.

	err = rtnl.SetOption(netlink.ExtendedAcknowledge, true)
	if err != nil {
		log.Printf("could not set option ExtendedAcknowledge: %v\n", err)
		return false
	}

	// get all the qdiscs from all interfaces
	qdiscs, err := rtnl.Qdisc().Get()
	if err != nil {
		log.Printf("could not get qdiscs: %v\n", err)
		return false
	}

	for _, qdisc := range qdiscs {
		iface, err := net.InterfaceByIndex(int(qdisc.Ifindex))
		if err != nil {
			log.Printf("could not get interface from id %d: %v", qdisc.Ifindex, err)
			continue
		}
		if iface.Index == netInterface.Index {
			if qdisc.Kind == "clsact" {
				return true
			}
		}
	}
	return false
}

// small helper to perform LE to BE in a uint16
func uint16LEToBE(le uint16) uint16 {
	bytes := make([]byte, 2)
	// Replace with NativeEndian when golang 1.21 is available
	binary.LittleEndian.PutUint16(bytes, le)

	be := binary.BigEndian.Uint16(bytes)

	return be
}

// small helper to perform BE to BE in an uint16
func uint16BEtoLE(be uint16) uint16 {
	bytes := make([]byte, 2)
	binary.BigEndian.PutUint16(bytes, be)

	le := binary.LittleEndian.Uint16(bytes)

	return le
}
