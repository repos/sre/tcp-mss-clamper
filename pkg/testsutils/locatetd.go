package testsutils

import (
	"errors"
	"os"
	"path/filepath"
)

var TestdataNotFoundErr = errors.New("unable to find testdata directory")

// GetTestdata attempts to find the testdata directory beginning on the
// current directory and going backwards. Git cannot be used here cause
// debian packaging builds and run tests outside a git checkout
func GetTestdata() (string, error) {
	tdCandidate, err := os.Getwd()
	if err != nil {
		return "", err
	}

	var tdPath string
	for tdPath != "/" {
		tdPath = filepath.Join(tdCandidate, "testdata")
		if _, err := os.Stat(tdPath); os.IsNotExist(err) {
			tdCandidate = filepath.Dir(tdCandidate)
		} else {
			return tdPath, nil
		}
	}

	return "", TestdataNotFoundErr
}
