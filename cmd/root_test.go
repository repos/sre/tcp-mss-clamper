//go:build linux
// +build linux

/*
Copyright 2023 Wikimedia Foundation Inc.
Copyright 2018 Google Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

	http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package cmd

import (
	"bytes"
	"fmt"
	"io/ioutil"
	"log"
	"net"
	"os"
	"os/exec"
	"os/signal"
	"path/filepath"
	"strings"
	"syscall"
	"testing"
	"time"

	"golang.org/x/sys/unix"

	"github.com/florianl/go-tc"
	"github.com/florianl/go-tc/core"

	"gitlab.wikimedia.org/repos/sre/go-qemutest/pkg/qemutest"

	"gitlab.wikimedia.org/repos/sre/tcp-mss-clamper/pkg/testsutils"
)

const (
	NETWORK_INTERFACE = "lo"
	SOURCE_IPV4       = "127.0.0.1"
	SOURCE_IPV6       = "[::1]"
	SOURCE_PORT       = "12345"
	MSS               = "1400"
)

// binaries that will be shipped on initrd
var initrdBinaries = []string{"/usr/sbin/bpftool"}

type QemuTest struct{}

func TestMain(m *testing.M) {
	if qemutest.InQemu() {
		// pinning requires /sys/fs/bpf
		if err := unix.Mount("bpf", "/sys/fs/bpf", "bpf", 0, ""); err != nil {
			log.Fatalf("failed to mount /sys/fs/bpf: %v", err)
		}
		// Before being able to use the eBPF program clsact qdisc
		// needs to be attached to the network interface
		iface, err := net.InterfaceByName(NETWORK_INTERFACE)
		if err != nil {
			log.Fatalf("unable to find interface %s: %v", NETWORK_INTERFACE, err)
		}
		tcnl, err := tc.Open(&tc.Config{})
		if err != nil {
			log.Fatalf("unable to open rtnetlink socket: %v", err)
		}
		qdisc := tc.Object{
			Msg: tc.Msg{
				Family:  unix.AF_UNSPEC,
				Ifindex: uint32(iface.Index),
				Handle:  core.BuildHandle(tc.HandleRoot, 0x0000),
				Parent:  tc.HandleIngress,
				Info:    0,
			},
			Attribute: tc.Attribute{
				Kind: "clsact",
			},
		}
		if err := tcnl.Qdisc().Add(&qdisc); err != nil {
			log.Fatalf("unable to attach qdisc: %v\n", err)
		}
	}
	os.Exit(qemutest.QemuTestMain(m))
}

func TestQemu(t *testing.T) {
	if qemutest.InQemu() {
		qemutest.RunQemuTests(t, QemuTest{})
		return
	}

	tdPath, err := testsutils.GetTestdata()
	if err != nil {
		t.Fatalf("unable to find testdata: %v", err)
	}

	testCases := []qemutest.QemuTestCase{
		{
			KernelImage: filepath.Join(tdPath, "bzImage.6.1"),
			TestId:      "6.1.69-bookworm",
			Verbose:     true,
		},
		{
			KernelImage: filepath.Join(tdPath, "bzImage.5.10"),
			TestId:      "5.10.197-bullseye",
			Verbose:     true,
		},
	}

	qemutest.RunQemu(t, testCases, initrdBinaries)
}

func isEbpfLoaded(t *testing.T, expect bool) bool {
	programFound := false
	pollingTime := 10 * time.Millisecond
	for i := time.Duration(0); i < time.Second; i += pollingTime {
		cmd := exec.Command("/usr/sbin/bpftool", "prog", "list", "name", "tcp_mss_clamper")
		_, err := cmd.CombinedOutput()
		if err == nil {
			programFound = true
		}
		if programFound && expect {
			return true
		}
		if !programFound && !expect {
			return false
		}
		time.Sleep(pollingTime)
	}
	if programFound {
		return true
	}
	if !programFound {
		t.Error("eBPF program not loaded after 1 second")
	}
	return false
}

func TestRootCmd(t *testing.T) {
	if qemutest.InQemu() {
		t.Skip("Check native execution")
	}
	outBytes := bytes.NewBufferString("")
	errBytes := bytes.NewBufferString("")
	cmd := NewRootCmd()
	cmd.SetOut(outBytes)
	cmd.SetErr(errBytes)
	cmd.Execute()
	stdout, err := ioutil.ReadAll(outBytes)
	if err != nil {
		t.Fatal(err)
	}
	stderr, err := ioutil.ReadAll(errBytes)
	if err != nil {
		t.Fatal(err)
	}
	testCases := []struct {
		flags  []string
		output string
	}{
		{
			flags:  []string{"interface", "ipv4-mss", "ipv6-mss"},
			output: string(stderr),
		},
		{
			flags:  []string{"prometheus", "help"},
			output: string(stdout),
		},
	}
	for _, testCase := range testCases {
		for _, flag := range testCase.flags {
			t.Run(flag, func(t *testing.T) {
				if !strings.Contains(string(testCase.output), flag) {
					t.Errorf("%s not listed on %s", flag, testCase.output)
				}
			})
		}
	}
}

func runCmd(t *testing.T) <-chan bool {
	done := make(chan bool, 1)
	go func() {
		cmd := NewRootCmd()
		args := []string{
			"-i", NETWORK_INTERFACE,
			"--ipv4-mss", MSS,
			"--ipv6-mss", MSS,
			"-s", fmt.Sprintf("%s:%s", SOURCE_IPV4, SOURCE_PORT),
			"-s", fmt.Sprintf("%s:%s", SOURCE_IPV6, SOURCE_PORT),
		}
		cmd.SetArgs(args)
		if err := cmd.Execute(); err != nil {
			t.Errorf("failed to execute the cmd: %v", err)
		}
		done <- true
	}()
	return done
}

func (QemuTest) TestSignalHandling(t *testing.T) {
	// Given that this test doesn't fork() if for some reason the cmd fails to install
	// the signal handler for SIGHUP or SIGTERM the go test binary will exit and test
	// reports could look weird. More details on default Go behavior in:
	// https://pkg.go.dev/os/signal#hdr-Default_behavior_of_signals_in_Go_programs
	// This can be avoided by installing a signal handler for this test
	sigChan := make(chan os.Signal, 1)
	signal.Notify(sigChan, syscall.SIGHUP, syscall.SIGTERM)
	testCases := []struct {
		name   string
		signal syscall.Signal
		// eBPF program is still loaded after signal
		ebpfLoaded bool
	}{
		{"SIGHUP", syscall.SIGHUP, true},
		{"SIGTERM", syscall.SIGTERM, false},
	}
	for _, testCase := range testCases {
		t.Run(testCase.name, func(t *testing.T) {
			done := runCmd(t)
			if !isEbpfLoaded(t, true) {
				t.Fatal("cmd failed to load ebpf program")
			}
			// poll till eBPF program is loaded
			wait := true
			for wait {
				// we can't assume that the signal handler is already in place so
				// we have two options. A static and relatively long time.Sleep()
				// or retry every 10ms and give up after a few seconds
				syscall.Kill(syscall.Getpid(), testCase.signal)
				select {
				case <-sigChan:
					time.Sleep(10 * time.Millisecond)
				case <-done:
					wait = false
				case <-time.After(5 * time.Second):
					t.Errorf("cmd didn't react to %v in 5 seconds", testCase.name)
					wait = false
				}
			}
			if loaded := isEbpfLoaded(t, testCase.ebpfLoaded); loaded != testCase.ebpfLoaded {
				t.Errorf("eBPF program is reported as loaded = %v, wanted = %v", loaded, testCase.ebpfLoaded)
			}
		})
	}
}
