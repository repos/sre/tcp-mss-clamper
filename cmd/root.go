/*
Copyright © 2023 Valentin Gutierrez

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>.
*/
package cmd

import (
	"context"
	"log"
	"net"
	"net/http"
	"net/netip"
	"os"
	"os/signal"
	"syscall"

	"github.com/mmatczuk/anyflag"
	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	"github.com/spf13/cobra"
	"gitlab.wikimedia.org/repos/sre/tcp-mss-clamper/pkg/clamper"
	"gitlab.wikimedia.org/repos/sre/tcp-mss-clamper/pkg/metrics"
)

var (
	ifaces                []*net.Interface
	sources               []netip.AddrPort
	v4MSS                 uint16
	v6MSS                 uint16
	prometheusBindAddress string
)

// rootCmd represents the base command when called without any subcommands
func NewRootCmd() *cobra.Command {
	cmd := &cobra.Command{
		Use: "tcp-mss-clamper",
		RunE: func(cmd *cobra.Command, args []string) error {
			var clMap map[string]*clamper.Clamper
			clMap = make(map[string]*clamper.Clamper)

			for _, iface := range ifaces {
				clMap[iface.Name] = clamper.NewClamper(v4MSS, v6MSS, iface)
				if err := clMap[iface.Name].Init(); err != nil {
					log.Printf("Unable to init eBPF program: %v", err)
					return err
				}
				defer clMap[iface.Name].Close()
				// Pin by default to be able to survive a CLI crash without impacting traffic
				clMap[iface.Name].Pin()

				for _, source := range sources {
					clMap[iface.Name].AddSource(source)
				}
			}

			ctxTerm, stopTerm := signal.NotifyContext(context.Background(), os.Interrupt, syscall.SIGTERM)
			defer stopTerm()
			ctxHup, stopHup := signal.NotifyContext(context.Background(), syscall.SIGHUP)
			defer stopHup()

			if prometheusBindAddress != "" {
				reg := prometheus.NewPedanticRegistry()
				reg.MustRegister(
					&metrics.ClamperMetricCollector{Programs: clMap},
					// Add the standard process and Go metrics to the custom registry
					prometheus.NewProcessCollector(prometheus.ProcessCollectorOpts{}),
					prometheus.NewGoCollector(),
				)
				go func() {
					http.Handle("/metrics", promhttp.HandlerFor(reg, promhttp.HandlerOpts{}))
					log.Fatal(http.ListenAndServe(prometheusBindAddress, nil))
				}()
			}

			select {
			case <-ctxHup.Done():
				log.Println("exiting (eBPF program still running)")
				return nil
			case <-ctxTerm.Done():
				log.Println("stopping eBPF program and exiting")
				for _, clamper := range clMap {
					clamper.Unpin()
				}
				return nil
			}

		},
	}
	cmd.Flags().Uint16Var(&v4MSS, "ipv4-mss", 0, "IPv4 MSS to be applied")
	cmd.Flags().Uint16Var(&v6MSS, "ipv6-mss", 0, "IPv6 MSS to be applied")
	cmd.Flags().StringVarP(&prometheusBindAddress, "prometheus", "p", "", "Prometheus bind address")
	addrPortSlice := anyflag.NewSliceValue[netip.AddrPort](nil, &sources, netip.ParseAddrPort)
	cmd.Flags().VarP(addrPortSlice, "source", "s", "source IP:port which will be TCP MSS clamped [can be specified multiple times]")
	interfaceSlice := anyflag.NewSliceValue[*net.Interface](nil, &ifaces, net.InterfaceByName)
	cmd.Flags().VarP(interfaceSlice, "interface", "i", "interfaces where TCP MSS clamping will happen [can be specified multiple times]")

	cmd.MarkFlagRequired("interface")
	cmd.MarkFlagRequired("ipv4-mss")
	cmd.MarkFlagRequired("ipv6-mss")
	cmd.MarkFlagRequired("source")

	return cmd
}

// Execute adds all child commands to the root command and sets flags appropriately.
// This is called by main.main(). It only needs to happen once to the rootCmd.
func Execute() {
	rootCmd := NewRootCmd()
	err := rootCmd.Execute()
	if err != nil {
		os.Exit(1)
	}
}
